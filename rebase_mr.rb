require 'faraday'
require 'faraday_middleware'
require 'json'

PRIVATE_TOKEN = ENV['private_token']

GITLAB_API_INSTAMCE = 'https://www.gitlab.com/api/v4/'

MRS_API = "#{GITLAB_API_INSTAMCE}merge_requests?state=opened&private_token=#{PRIVATE_TOKEN}&approved_by_ids=Any"

CONNECTION = Faraday.new { |b|
  b.use FaradayMiddleware::FollowRedirects
  b.adapter :net_http
}

# Trigger a rebase job on GitLab
def rebase(merge_request)
  rebase_response = JSON.parse((CONNECTION.put "#{GITLAB_API_INSTAMCE}/projects/#{merge_request['project_id']}/merge_requests/#{merge_request['iid']}/rebase?private_token=#{PRIVATE_TOKEN}").body)

  if rebase_response['rebase_in_progress']
    puts 'Rebase queued'
  else
    puts 'Something went wrong'
  end
end

# Returns true if the given MR is 100 commits away from gitlab master
def is_mr_100_away_from_master?(merge_request)
  mr_response = JSON.parse((CONNECTION.get "#{GITLAB_API_INSTAMCE}/projects/#{merge_request['project_id']}/merge_requests/#{merge_request['iid']}?private_token=#{PRIVATE_TOKEN}&include_diverged_commits_count=true").body)
  return mr_response['diverged_commits_count'] >= 100
end

response = JSON.parse((CONNECTION.get MRS_API).body)

response.each do |mr| 
  if mr['has_conflicts'] || mr['merge_when_pipeline_succeeds']
    puts "Skipping branch: #{mr['source_branch']} due to conflicts/automatic pipeline"
  elsif is_mr_100_away_from_master?(mr)
    puts "Rebasing #{mr['source_branch']}"
    rebase(mr)
  else
    puts "Rebasing not required for #{mr['source_branch']} because MR is not 100 commits away from master"
  end
end
