# GitLab MR Rebaser

Rebases open MRs to GitLab master if the source branch is [100 commits away](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request) from master and approved by a reviewer/maintainer.

## How to run?

1. Fork this repo to your account.
2. Set your `private_token` as a custom variable in pipeline settings. [Read](https://docs.gitlab.com/ee/ci/variables/#custom-environment-variables) for more information on how to do it.
3. Schedule the pipeline using [GitLab pipeline schedule.](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
4. You are all set.
